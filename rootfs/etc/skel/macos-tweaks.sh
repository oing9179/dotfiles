#!/bin/bash
# Turn off animations..
# Opening and closing windows and popovers
defaults write -g NSAutomaticWindowAnimationsEnabled -bool false
# Smooth scrolling
defaults write -g NSScrollAnimationEnabled -bool false
# Window resize, float 0 doesn't work.
defaults write -g NSWindowResizeTime -float 0.001
# Opening and closing Quick Look windows
defaults write -g QLPanelAnimationDuration -float 0
# Rubberband scrolling (doesn't affect web views)
defaults write -g NSScrollViewRubberbanding -bool false
# Resizing windows before and after showing the version browser
# also disabled by NSWindowResizeTime -float 0.001
defaults write -g NSDocumentRevisionsWindowTransformAnimation -bool false
# Showing a toolbar or menu bar in full screen
defaults write -g NSToolbarFullScreenAnimationDuration -float 0
# Scrolling column views
defaults write -g NSBrowserColumnAnimationSpeedMultiplier -float 0
# Auto-hide antimation of the Dock
defaults write com.apple.dock autohide-time-modifier -float 0
defaults write com.apple.dock autohide-delay -float 0
# Showing and hiding Mission Control, command+numbers
defaults write com.apple.dock expose-animation-duration -float 0
# Showing and hiding Launchpad
defaults write com.apple.dock springboard-show-duration -float 0
defaults write com.apple.dock springboard-hide-duration -float 0
# Changing pages in Launchpad
defaults write com.apple.dock springboard-page-duration -float 0.1
# At least AnimateInfoPanes
defaults write com.apple.finder DisableAllAnimations -bool true
# Sending messages and opening windows for replies
defaults write com.apple.Mail DisableSendAnimations -bool true
defaults write com.apple.Mail DisableReplyAnimations -bool true

# Always display scrollbar
defaults write -g AppleKeyboardUIMode -string Always

# Tweaks for Finder
defaults write -g AppleShowAllExtensions -int 1
defaults write com.apple.Finder AppleShowAllFiles -bool true
defaults write com.apple.Finder ShowRecentTags -int 0
defaults write com.apple.Finder AppleKeyboardUIMode -string Nlsv
defaults write com.apple.Finder ShowPathbar -int 1
defaults write com.apple.Finder ShowSidebar -int 1
defaults write com.apple.Finder ShowStatusBar -int 1
# Default view is list view
defaults write com.apple.Finder FXPreferredViewStyle -string Nlsv
defaults write com.apple.Finder FXEnableExtensionChangeWarning -int 0
defaults write com.apple.Finder "_FXSortFoldersFirst" -int 1
defaults write com.apple.Finder NewWindowTarget -string PfHm
defaults write com.apple.Finder NewWindowTargetPath -string "file://$HOME/"

# No Siri on Status Menu Bar
defaults write com.apple.Siri StatusMenuVisible -int 0

# Launchpad tweaks
defaults write com.apple.dock tilesize -int 24
defaults write com.apple.dock mineffect -string scale
defaults write com.apple.dock size-immutable -int 1
defaults write com.apple.dock launchanim -int 0
defaults write com.apple.dock magnification -int 0
defaults write com.apple.dock "show-recents" -int 0

# Keyboard tweaks
# Enable faster key repeat
defaults write -g ApplePressAndHoldEnabled -bool false
defaults write -g InitialKeyRepeat -int 15
defaults write -g KeyRepeat -int 2
# Hold Fn key to show function keys on touchbar
defaults delete -g AppleFnUsageType
# Enable keyboard navigation
defaults write -g AppleKeyboardUIMode 2

# No more red dots on the icon of the System Preferences app.
defaults write com.apple.systempreferences AttentionPrefBundleIDs 0

# Touchbar tweaks
#"com.apple.touchbar.agent" = {
#    PresentationModeFnModes = {
#        appWithControlStrip = functionKeys;
#    };
#    # PresentationModeGlobal = appWithControlStrip;
#};
defaults delete com.apple.touchbar.agent PresentationModeGlobal
# Display function keys as soon as I press the Fn key.
defaults write com.apple.HIToolbox AppleFnUsageType -int 0

# Disable mouse acceleration
defaults write -g com.apple.mouse.scaling -integer -1

killall Finder
killall Dock
