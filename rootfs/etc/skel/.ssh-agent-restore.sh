#!/bin/bash

sa_exports_path="$HOME/.ssh-agent-exports";
is_sa_unreachable=1;

if test -f "$sa_exports_path"; then
  printf "SSH Agent: ";
  source $sa_exports_path;
fi;

if test -n "$SSH_AGENT_PID"; then
  ps -p $SSH_AGENT_PID 2>&1 > /dev/null;
  if test $? -eq 0; then
    is_sa_unreachable=0;
  fi
fi

sa_exports="";

if test $is_sa_unreachable -ne 0; then
    echo "Dead ssh-agent, restarting..";
    killall ssh-agent 2>/dev/null || true;
    sa_exports="$(ssh-agent -s)";
    echo $sa_exports > $sa_exports_path;
    printf "SSH Agent: ";
    source $sa_exports_path;
fi;

