export PATH=$HOME/.local/bin:$PATH;
source $HOME/.aliases
source $HOME/.exports
if [[ $OSTYPE == 'darwin'* ]]; then
  source $HOME/.exports-macos;
fi
source $HOME/.bash_prompt
source $HOME/.ssh-agent-restore.sh
if test -f "/usr/share/bash-completion/bash_completion"; then
  source /usr/share/bash-completion/bash_completion;
fi

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob;
# Append to the Bash history file, rather than overwriting it
shopt -s histappend;
# Autocorrect typos in path names when using `cd`
shopt -s cdspell;
# Auto-completion ignore case.
set completion-ignore-case on;
